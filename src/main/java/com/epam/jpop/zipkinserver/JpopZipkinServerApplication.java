package com.epam.jpop.zipkinserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import zipkin.server.EnableZipkinServer;


@SpringBootApplication
@EnableZipkinServer
public class JpopZipkinServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(JpopZipkinServerApplication.class, args);
	}

}
